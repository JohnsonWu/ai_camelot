from threading import Thread
from queue import Queue, Empty
import time


def worker(q):
    while True:

        try:
            item = q.get(block=True, timeout=1)

            q.task_done()
            if item == "quit":
                print("got quit msg in thread")
                break

        except Empty:
            print("empty, do some arduino stuff")


def input_process(q):
    while True:
        x = input("")

        if x == 'q':
            print("will quit")
            q.put("quit")
            break

q = Queue()
t = Thread(target=worker, args=(q,))
t.start()

t2 = Thread(target=input_process, args=(q,))
t2.start()

# waits for the `task_done` function to be called
q.join()

t2.join()
t.join()

# ---------------------------------------------------------- #
# def threaded(fn):
#     def wrapper(*args, **kwargs):
#         thread = Thread(target=fn, args=args, kwargs=kwargs)
#         thread.start()
#         return thread
#     return wrapper

# class MyClass():
#     somevar = 'someval'

#     @threaded
#     def func_to_be_threaded(self):
#         for i in range(5):
#             time.sleep(1)
#             print('xyz')

#     @threaded
#     def func_to_be_threaded2(self):
#         for i in range(5):
#             time.sleep(2)
#             print('abc')

# if __name__ == "__main__":
#     my_obj = MyClass()
#     handle = my_obj.func_to_be_threaded()
#     handle2 = my_obj.func_to_be_threaded2()

#     handle2.join()
#     handle.join()

# ---------------------------------------------------------- #

from queue import Queue
 
# Initializing a queue
q = Queue(maxsize = 3)
 
# qsize() give the maxsize
# of the Queue
print(q.qsize())
 
# Adding of element to queue
q.put('a')
q.put('a')
q.put('a')
 
# Return Boolean for Full
# Queue
print("\nFull: ", q.full())
 
# Removing element from queue
print("\nElements dequeued from the queue")
print(q.get())
print(q.get())
print(q.get())
 
# Return Boolean for Empty
# Queue
print("\nEmpty: ", q.empty())
 
q.put(1)
print("\nEmpty: ", q.empty())
print("Full: ", q.full())
 
# This would result into Infinite
# Loop as the Queue is empty.
# print(q.get())